#Funkcija provjerava je li riječ palindrom; primjer_07_03
def palindrom(s):
    '''Funkcija provjerava je li riječ palindrom rijesen problem veliko malo slovo'''
    return s.upper() == s[::-1].upper()
	
def palindrom1(s):
    '''Funkcija provjerava je li riječ palindrom rijesen problem veliko malo slovo i razmaci'''
    return s.upper().replace(' ','') == s[::-1].upper().replace(' ','')

def palindrom2(s):
    '''Funkcija provjerava je li riječ palindrom rijesen problem veliko malo slovo, razmaci i slova lj nj i dž'''
    s = s.replace('lj','1').replace('nj','2').replace('dž','3').replace('LJ','1').replace('NJ','2').replace('DŽ','3')
    return s.upper().replace(' ','') == s[::-1].upper().replace(' ','')
