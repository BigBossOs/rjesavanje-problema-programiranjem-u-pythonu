#Za uneseni broj zelimo ustanoviti je li manji od nule, jednak nuli ili veci od nje
#Izbor jedne od tri mogucnosti s if-elif-else strukturom; primjer_05_02_B

broj=int(input('Utipkati zeljeni broj: '))
if broj == 0:
    print('Broj je jednak nuli')
elif broj > 0:
    print('Broj je veci od nule')
else:
    print('Broj je manji od nule')
