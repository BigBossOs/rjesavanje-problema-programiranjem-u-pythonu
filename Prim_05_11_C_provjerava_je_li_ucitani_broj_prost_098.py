# Provjerava je li učitani broj prost; primjer_05_11_c
import math
n =int(input('Unesi broj: '))
brojac = 0
for i in range(2, round(math.sqrt(n)+1)):
    if n % i ==0:
        brojac += 1
if brojac == 0 and n>1:
    print('Broj {} je prost'.format(n))
else:
    print('Broj {} nije prost'.format(n))
