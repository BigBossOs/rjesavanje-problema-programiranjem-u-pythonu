# Upotreba if-else strukture na drugi način; primjer_05_01_B
print('Program će ispisati je li neki utipkani broj djeljiv sa 7')
broj=int(input('Utipkaj prirodni broj: '))
if broj % 7 == 0:
    glagol='je'
else:
   glagol='nije'
print('Broj {} {} djeljiv sa sedam.'.format(broj,glagol))
