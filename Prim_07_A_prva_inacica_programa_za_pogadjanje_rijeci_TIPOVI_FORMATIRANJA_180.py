# Prva inačica programa za pogađanje rjieči; primjer_07_A
s1 = '''          Program zadaje nepoznatu riječ.
          Igrač pogađa zadanu riječ utipkavanjem pojedinačnih
          malih slova hrvatske abecede.
          Broj dopuštenih pokušaja jednak je udvostručenom
          broju različitih slova u riječi.
          Igra se može ponoviti najviše {0} puta
          jer program ima toliko nepoznatih riječi.'''
s2 = '''Dopušta se najviše {0} pokušaja pogađanja slova.
Ne uračunavaju se neprihvatljivi znaci i već utipkana slova.

                                    Pogođena slova:
                                    Pogrešna slova:

Nepoznata riječ: {1}
'''
najveci_broj_igara = 7
broj_igre = 1
zadana_riječ = 'ruža'
najveci_broj_pog = 2 * len(zadana_riječ)
pogodena_riječ = '_ ' * len(zadana_riječ)

print(s1.format(najveci_broj_igara))
print('\n' + 30 * '-', '{0}. IGRA'.format(broj_igre), 30 * '-' + '\n')
print(s2.format(najveci_broj_pog, pogodena_riječ))


# načini zapisa funkcije print
# gornji način
print('\n' + 30 * '-', '{0}. IGRA'.format(broj_igre), 30 * '-' + '\n')

# drugi način
# print('\n{:'-' * 67}\n'.format(' {0}. IGRA'.format(broj_igre)))
# treći način
s3 = '\n------------------------------ {}. IGRA ------------------------------\n'
print(s3.format(broj_igre))
      
# četvrti način
s3 = '\n' + '-' *30 + ' {}. IGRA ' + '-' * 30 + '\n'
print(s3.format(broj_igre))
