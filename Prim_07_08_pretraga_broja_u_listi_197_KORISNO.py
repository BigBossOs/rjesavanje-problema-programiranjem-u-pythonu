# Program generira listu od 20 nasumicnih brojeva do 10 i za unos prirodnog broja t kaze na kojim se mjestima isti pojavljuje
# prim_07_08_B
from random import *
def vrati_indekse(a, t):
    indeksi = []
    for i in range (len(a)):
        if a[i] == t:
            indeksi += [i]
    return indeksi

def main():
    a = [randint(1, 10) for i in range(20)]
    print(a)
    t = int(input('Upisi broj: '))
    print(vrati_indekse(a, t))

main()
