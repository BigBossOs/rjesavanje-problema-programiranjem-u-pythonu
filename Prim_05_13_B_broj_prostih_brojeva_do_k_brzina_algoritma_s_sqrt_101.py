# Broji koliko ima prostih brojeva među prvih k prirodnih brojeva; primnjer_05_13_B
import math
import time
k = int(input('Unesi broj: '))
brojac = 0
pocetak = time.clock()
for n in range(2, k+1):
    nema_djelitelja = True
    for i in range(2, round(math.sqrt(n)+1)):
        if n % i == 0:
            nema_djelitelja = False
    if nema_djelitelja:
        brojac += 1
kraj = time.clock()
print('Medu prvih {} prirodnih brojeva ima {} prostih brojeva.'.format(k, brojac))
print('Program se izvodio {:.4f} sekundi.'.format(kraj-pocetak))
