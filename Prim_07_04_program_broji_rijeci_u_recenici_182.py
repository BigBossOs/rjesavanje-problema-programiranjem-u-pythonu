# Program broji riječi u rečenici; primjer_07_04
def broj_riječi(s):
    s = s.strip()
    while '  ' in s:
        s = s.replace('  ',' ')
    broj_razmaka = 0
    for i in range(len(s)):
        if s[i] == ' ':
            broj_razmaka += 1
    return broj_razmaka + 1
