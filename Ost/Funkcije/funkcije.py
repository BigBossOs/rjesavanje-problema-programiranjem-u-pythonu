# FUNKCIJE
import sys
sys.path.append(r'C:\Users\Bartol\Desktop\Tuts\Python\Rjesavanje problema u Pythonu\Ost\Funkcije')
import funkcije


def savrsen(n):
    '''Funkcija vraća True ako je n savršen broj dok u suprotnom vraća False'''
    sum = 0
    for i in range(1,n):
        if not n % i:
            sum += i
    if sum == n:
        return True
    else:
        return False   

def savrsen_n(n):
    '''Funkcija ispisuje sve savršene brojeve do n'''
    b = 0
    for i in range(1,n+1):
        if savrsen(i):
            b += 1
            print(i)
    if b == 0:
        print('NEMA!')
    return  

def prost(n):
    '''Funkcija vraća True ako je n prost broj dok u suprotnom vraća False'''
    nema_djelitelja = True
    for i in range(2, round(n ** (1/2) +1)):
        if n % i == 0:
            nema_djelitelja = False
    if nema_djelitelja and n>1:
        return True
    else:
        return False
    
def prost_n(n):
    '''Funkcija ispisuje sve proste brojeve do n'''
    b = 0
    for i in range(1,n+1):
        if prost(i):
            b += 1
            print(i)
    if b == 0:
        print('NEMA!')
    return
            
        
def armstrongov(n):
    '''Funkcija vraća True ako je n Armstrongov broj dok u suprotnom vraća False'''
    t, s, p = n, 0, 0
    while n > 0:
        n //= 10
        p += 1
    n = t
    while n > 0:
        s += (n % 10) ** p
        n //= 10
    if s == t:
        return True
    else:
        return False

def armstrongov_n(n):
    '''Funkcija ispisuje sve Armstrongove brojeve do n'''
    b = 0
    for i in range(1, n + 1): 
        if armstrongov(i):
            b += 1
            print(i)
    if b == 0:
        print('NEMA')
    return


