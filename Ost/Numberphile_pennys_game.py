n = int(input('Koliko puta želiš ponoviti bacanje novcica: '))
you = input('Upisi slijed tri bacanja novcica u formatu T-pismo H-glava kao npr TTH ili HHT: ').upper()
if you[1]== 'T':
    me = 'H'+you[0]+you[1]
elif you[1] == 'H':
    me = 'T'+you[0]+you[1]

my_wins = 0
your_wins = 0

from random import *
for i in range(n):
    slijed = ""
    while you not in slijed and me not in slijed:
        slijed += choice(['T','H'])
        if me in slijed:
            my_wins += 1
        elif you in slijed:
            your_wins += 1
    print('{:3d}. bacanje: {}'.format(i + 1,slijed))   
print('Ti si odabrao: {}\nJa sam odabrao: {}'.format(you, me))
print('\nREZULTAT NAKON {} PONAVLJANJA:\nJa sam pobjedio: {} puta\nTi si pobjedio: {} puta'.format(n, my_wins, your_wins))
print('Pobijedio sam u {}% posto slučajeva'.format(round(my_wins/n*100)))
      



