from random import *
def ponovi(n):
    # Generiranje svih brojeva od 1 do 45
    brojevi = set(i + 1 for i in range(45))
    # Odabir 6 različitih nasumičnih brojeva do 45
    kombinacija = set(sample(brojevi, 6))
    kombinacija_rand = set(sample(brojevi, 6))
    b = 0
    while len(kombinacija & kombinacija_rand) < n:
        kombinacija_rand = set(sample(brojevi, 6))
        pogodeni = kombinacija & kombinacija_rand
        b += 1
    return b

def main():
    n = int(input('Koliko brojeva u LOTU 6/45 želiš da program pogodi: '))
    m = int(input('Koliko puta želiš ponavljati igru: '))
    brojac = 0
    for i in range(m):
        brojac += ponovi(n)
    print('U prosjeku se zgoditak ostvaruje nakon {} ponavljanja'.format(brojac/m))
    input('Enter za unos')
    return

main()


