from random import choice
from time import clock
def pobjednik(you, me):
    i = 0
    slijed = ""
    while you not in slijed and me not in slijed:
        slijed += choice(['T','H'])
        i += 1
    if me in slijed:
        proglas = 'Moja'
    else:
        proglas = 'Tvoja'
    return bool(me in slijed), slijed, i, proglas

def main():
    pocetak = clock()
    my_wins = 0
    your_wins = 0
    suma_mojih_bacanja = 0
    suma_tvojih_bacanja = 0
    max_bacanja = 0
    n = int(input('Koliko rundi želiš odigrati?: '))
    you = input('Upisi slijed tri bacanja novcica u formatu T-pismo H-glava kao npr TTH ili HHT: ').upper()
    if you[1]== 'T':
        me = 'H'+you[0]+you[1]
    elif you[1] == 'H':
        me = 'T'+you[0]+you[1]
    for i in range(n):
        ja, bacanje, brojac, proglas = pobjednik(you, me)
        if ja:
            my_wins += 1
            suma_mojih_bacanja += brojac
        else:
            your_wins += 1
            suma_tvojih_bacanja += brojac
        if brojac > max_bacanja:
            max_bacanja = brojac
            broj_runde = i + 1
        print('{:3d}. runda: {:30s}Poklapanje nakon {} bacanja     \t{} pobjeda'.format(i + 1, bacanje, brojac, proglas))
    ukupna_bacanja = suma_mojih_bacanja + suma_tvojih_bacanja
    print('\nODABIR:\nTi si odabrao: {}\nJa sam odabrao: {}'.format(you, me))
    print('\nREZULTAT NAKON {} PONAVLJANJA:\nMoje pobjede: {} puta\nTvoje pobjede: {} puta'.format(n, my_wins, your_wins))
    print('Pobijedio sam u {}% posto slučajeva'.format(round(my_wins/n*100)))
    print('U prosjeku je svaka runda u kojoj sam ja podijedio imala {:.2f} bacanja'.format(suma_mojih_bacanja/my_wins))
    print('U prosjeku je svaka runda u kojoj si ti podijedio imala {:.2f} bacanja'.format(suma_tvojih_bacanja/your_wins))
    print('Bilo je najviše {} bacanja i to u {}. rundi'.format(max_bacanja, broj_runde))
    print('U svim rundama zajedno bilo je {} bacanja'.format(ukupna_bacanja))
    kraj = clock()
    print('Simulacija je trajala {:.2f} sekundi'.format(kraj - pocetak))
    input('Enter za kraj')
    return

if __name__ == '__main__':
    main()
