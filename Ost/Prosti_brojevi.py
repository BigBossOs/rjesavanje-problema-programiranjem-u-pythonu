from math import sqrt
lower, upper = int(input("Unesi donji prag: ")), int(input("Unesi gornji prag: "))
brojac = zbroj_brojeva = 0
for num in range(lower,upper + 1):
	prime=True
	for i in range(2,round(sqrt(num))+1):
		if (num%i)==0:
			prime= False
	if prime==True and num>1:
		print(num,'je prime')
		brojac += 1
		zbroj_brojeva += num
	else:
		print(num, 'nije prime')
print('Izmedu brojeva {} i {} ima {} prostih brojeva i zbroj im je {}.'.format(lower, upper, brojac, zbroj_brojeva))
