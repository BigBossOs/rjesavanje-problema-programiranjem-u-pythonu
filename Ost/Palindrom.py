def palindrom(s):
    '''Funkcija provjerava je li riječ palindrom
    1. rijesen problem veliko - malo slovo, 
    2. rijen razmak i visestruki razmaci
    3. rijesen problem slova 'lj', 'nj', 'dž'!'''
    s = s.strip()
    while '  ' in s:
    	s = s.replace('  ',' ')
    s = s.replace('lj','1').replace('nj','2').replace('dž','3').replace('LJ','1').replace('NJ','2').replace('DŽ','3')
    return s.upper().replace(' ','') == s[::-1].upper().replace(' ','')
 

