# Tablica množenja do n
n = int(input('Upisi jedan broj: '))
print('    * |',end = '')
for i in range(1, n+1):
    print('{:5}'.format(i), end = '') #Ovaj red u knjizi ima bug end = ' '
print()
print('------+',end ='')
for i in range(1,n+1):
    print('---', end = '')
print()
for i in range (1, n+1):
    print('{:5} |'.format(i), end = '')
    for j in range (1, n+1):
        print('{:5}'.format(i*j), end = '')
    print()
