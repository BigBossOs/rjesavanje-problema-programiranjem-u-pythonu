# funkcija ispisuje sve proste armstrongove brojeve; primjer_06_04
def prost(n):
    nema_djelitelja = True
    for i in range(2, round(n ** (1/2) +1)):
        if n % i == 0:
            nema_djelitelja = False
    if nema_djelitelja and n>1:
        return True
    else:
        return False

def armstrongov(n):
    '''Funkcija vraća True ako je n Armstrongov, a  inače vrać false'''
    t, s, p = n, 0, 0
    while n > 0:
        n //= 10
        p += 1
    n = t
    while n > 0:
        s += (n % 10) ** p
        n //= 10
    if s == t:
        return True
    else:
        return False

def main():
    '''Funkcija unosi prirodan broj n i ispisuje sve Armstrongove brojeve do n'''
    n = int(input('Unesi prirodan broj: '))
    b = 0
    for i in range(10, n + 1): #ne provjerava od 1 jer su 2,3,5,7 prosti i armstrongovi a knjiga kaze da ne postoji broj koji je i prost i armstrongov
        if armstrongov(i) and prost(i):
            b += 1
            print(i)
    if b == 0:
        print('NEMA')
    return

if __name__ == '__main__':
    main()
