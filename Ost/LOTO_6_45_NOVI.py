from random import *
def ponovi(n):
    # Generiranje svih brojeva od 1 do 45
    brojevi = set(i + 1 for i in range(45))
    # Odabir 6 različitih nasumičnih brojeva do 45
    kombinacija = set(sample(brojevi, 6))
    kombinacija_rand = set(sample(brojevi, 6))
    b = 0
    while len(kombinacija & kombinacija_rand) < n:
        kombinacija_rand = set(sample(brojevi, 6))
        pogodeni = kombinacija & kombinacija_rand
        if pogodeni:
            print('pogođeno je {} brojeva i to su: {}'.format(len(pogodeni), pogodeni))
        else:
            print('Nije pogođen niti jedan broj')
        b += 1
    print('Bilo je porebno {} ponavljanja'.format(b))
    print('Odabrana kombinacija: {}'.format(kombinacija_rand))
    print('Dobitna kombinacija: {}'.format(kombinacija))
    print()
    return b

def main():
    n = int(input('Koliko brojeva u LOTU 6/45 želiš da program pogodi: '))
    m = int(input('Koliko puta želiš ponavljati igru: '))
    brojac = 0
    for i in range(m):
        print('{}. SIMULACIJA'.format(i+1))
        brojac += ponovi(n)
    print('U prosjeku se zgoditak ostvaruje nakon {} ponavljanja'.format(brojac/m))
    return

main()


