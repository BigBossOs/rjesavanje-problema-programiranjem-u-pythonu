from random import *
def main():
    n = int(input('Koliko brojeva želiš pogađati: '))
    # Generiranje svih brojeva od 1 do 45
    brojevi = set(i + 1 for i in range(45))
    # Odabir 6 različitih nasumičnih brojeva do 45
    kombinacija = set(sample(brojevi, 6))
    kombinacija_rand = set(sample(brojevi, 6))
    b = 0
    while len(kombinacija & kombinacija_rand) < n:
        kombinacija_rand = set(sample(brojevi, 6))
        pogodeni = kombinacija & kombinacija_rand
        if pogodeni:
            print('pogođeno je {} brojeva i to su: {}'.format(len(pogodeni), pogodeni))
        else:
            print('Nije pogođen niti jedan broj')
        b += 1
    print('Bilo je porebno {} ponavljanja'.format(b))
    print('Odabrana kombinacija: {}'.format(kombinacija_rand))
    print('Dobitna kombinacija: {}'.format(kombinacija))
    return

main()


