#Funkcija iza svakog samoglasnika dodaje - osim ako je samoglasnik na zadnjem mjestu; primjer_07_02

def palindrom(s):
    t = ''
    for i in range(len(s)):
        t = s[i] + t
    return t == 2

def palindrom2(s):
    for i in range(len(s)):
        if s[i] != s[-(i + 1)]:
            return False
    return True
        
    
