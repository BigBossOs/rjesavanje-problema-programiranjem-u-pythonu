# Unosi tri broja i ispisuje ih poredane po veličini; primjer_05_06
print('Unosi tri broja i ispisuje ih poredane po veličini')
a = int(input('Prvi broj: '))
b = int(input('Drugi broj: '))
c = int(input('Treci broj: '))
if a > b:
    a, b = b, a
if a > c:
    a, c = c, a
if b > c:
    b, c = c, b
print('Brojevi poredani po veličini od najmanjeg ka najvećem: {}, {}, {}'.format(a,b,c))
