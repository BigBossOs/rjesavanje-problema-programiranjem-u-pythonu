# Zaustavljanje while petlje upisivanjem negativnog broja; primjer_05_15_C
print('Izračunavanje srednje visine proizvoljnog broja osoba.')
brojilo = 0
zbroj = 0.0
broj = float(input('Upisati visinu u metrima (negativni broj za kraj): '))
while broj > 0:
    zbroj += broj
    brojilo += 1
    broj = float(input('Upisati visinu u metrima (negativni broj za kraj): '))
print('\nSrednja visina {} osoba u metrima je {:.2f}'.format(brojilo, zbroj / brojilo))

