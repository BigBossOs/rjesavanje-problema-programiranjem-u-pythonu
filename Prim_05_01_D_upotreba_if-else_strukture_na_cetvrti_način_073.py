# Upotreba if-else strukture na četvrti način; primjer_05_01_D
print('Program će ispisati je li neki utipkani broj djeljiv sa 7')
broj=int(input('Utipkaj prirodni broj: '))
if broj % 7:
    glagol='nije'
else:
    glagol='je'
print('Broj {} {} djeljiv sa sedam.'.format(broj,glagol))
