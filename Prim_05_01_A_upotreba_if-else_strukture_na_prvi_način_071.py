# Upotreba if-else strukture na prvi način; primjer_05_01_A
print('Program će ispisati je li neki utipkani broj djeljiv sa 7')
broj=int(input('Utipkaj prirodni broj: '))
if broj % 7 == 0:
    print('Broj {} je djeljiv sa sedam.'.format(broj))
else:
    print('Broj {} nije djeljiv sa sedam.'.format(broj))
