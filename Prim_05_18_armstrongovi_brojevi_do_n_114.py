# Ispis Armstrongovih brojeva do n; primjer_05_18
k = int(input('Unesi broj: '))
for n in range(1, k+1):
    a, s, p = n, 0, 0
    while n > 0:
        n //=10
        p += 1
    n = a
    while n > 0:
        j = n % 10
        n //= 10
        s += j**p
    if a == s:
        print(a, end=' ')

