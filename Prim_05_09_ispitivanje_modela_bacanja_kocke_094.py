# Ispitivanje modela bacanja kocke; primjer_05_09
from random import randint
print('''Program modelira bacanje kocke s N bacanja i ispisuje
dobivene vjerojatnosti brojeva v_1, v_2, v_3, v_4, v_5 i v_6.''')
N = int(input('Upisati željeni broj bacanja kocke N: '))
broj_1 = broj_2 = broj_3 = broj_4 = broj_5 = broj_6 = 0
for i in range (N):
    bac = randint(1,6)
    if bac == 1:
        broj_1 += 1
    elif bac == 2:
        broj_2 += 1
    elif bac == 3:
        broj_3 += 1
    elif bac == 4:
        broj_4 += 1
    elif bac == 5:
        broj_5 += 1
    else:
        broj_6 += 1
v_1, v_2, v_3, v_4, v_5, v_6 = broj_1 / N, broj_2 / N, broj_3 / N, broj_4 / N, broj_5 / N, broj_6 / N,
print('Za {0} bacanja kocke dobivene su sljedeće vjerojatnosti: \n1: {1}\n2: {2}\n3: {3}\n4: {4}\n5: {5}\n6: {6}'.format(N, v_1, v_2, v_3, v_4, v_5, v_6))
