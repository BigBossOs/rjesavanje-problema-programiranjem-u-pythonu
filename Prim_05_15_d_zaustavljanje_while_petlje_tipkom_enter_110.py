# Zaustavljanje while petlje tipkom <Enter>; primjer_05_15_D
print('Izračunavanje srednje visine proizvoljnog broja osoba.')
brojilo = 0
zbroj = 0.0
broj = input('Upisati visinu u metrima, <Enter> za kraj: ')
while broj != '':
    zbroj += float(broj)
    brojilo += 1
    broj = input('Upisati visinu u metrima, <Enter> za kraj: ')
print('\nSrednja visina {} osoba u metrima je {:.2f}'.format(brojilo, zbroj / brojilo))

