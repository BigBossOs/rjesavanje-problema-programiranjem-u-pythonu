# Islata iznosa u apoenima: 10, 5, 2, 1
n = int(input('Unesi iznos koji je potrebno razmjeniti u apoene 10, 5, 2, 1 sa što manje novcanica: '))
if n > 10:
    print('{} od 10'.format(n//10))
    n = n % 10
if n > 5:
    print('{} od 5'.format(n//5))
    n = n % 5
if n > 2:
    print('{} od 2'.format(n//2))
    n = n % 2
if n > 0:
    print('{} od 1'.format(n))
