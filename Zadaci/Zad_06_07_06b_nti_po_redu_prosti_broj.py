# Zad_06_07_06a nti po redu prosti brojevi

def prost(n):
    prost = True
    for i in range (2, round(n ** (1 / 2) + 1)):
        if n%i == 0:
            prost = False
    return prost

def prosti_nti_po_redu(n):
    t = 0
    i = 0
    while (t < n + 1):
        i += 1
        if prost(i):
            t += 1
    return i

def main():
    n = int(input('Unesi broj: '))
    print('{}. po redu prosti broj je {}'.format(n,prosti_nti_po_redu(n)))
    return

if __name__ == '__main__':
    main()
