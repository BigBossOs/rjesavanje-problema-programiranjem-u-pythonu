# Heronova formula za povrsinu torkuta
import math
a = int(input('Unesi prvu stranicu trokuta: '))
b = int(input('Unesi drugu stranicu trokuta: '))
c = int(input('Unesi trecu stranicu trokuta: '))
if a + b > c and a + c > b and b + c > a:
    s = (a + b + c) / 2
    p = math.sqrt(s * (s - a) * (s - b) * (s - c))
    print('Povrsina trokuta sa stranicama {}, {} i {} je {} cm2'.format(a, b, c, p))
else:
    print('{}, {} i {} ne mogu biti stranice trokuta')
