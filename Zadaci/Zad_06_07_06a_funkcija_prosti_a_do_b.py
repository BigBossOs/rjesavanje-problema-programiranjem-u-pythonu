Zad_06_07_06_a prosti brojevi između a i b
def prost(n):
    prost = True
    for i in range (2, round(n ** (1 / 2) + 1)):
        if n%i == 0:
            prost = False
    return prost


def prosti_od_a_do_b(a, b):
    t = 0
    for i in range(a, b + 1):
        if prost(i):
            t += 1
    return t

def main():
    a = int(input('Unesi prvi broj: '))
    b = int(input('Unesi drugi broj: '))
    print('Izmedu {} i {} je {} prostih brojeva'.format(a, b, prosti_od_a_do_b(a, b)))
    return

if __name__ == '__main__':
    main()
