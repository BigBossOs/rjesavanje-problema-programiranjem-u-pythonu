def zbroj_n(n):
    '''Funkcija vraća zbroj prvih n prirodnih brojeva'''
    s = 0
    for i in range(1,n+1):
        s += i
    return s

def main():
    n = int(input('Unesi broj: '))
    print('Zbroj prvih', n, 'brojeva je', zbroj_n(n))
    return

if __name__ == '__main__':
    main()
