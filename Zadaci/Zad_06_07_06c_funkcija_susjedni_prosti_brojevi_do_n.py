# Zad_06_07_06c susjendi prosti brojevi do n

def prost(n):
    prost = True
    for i in range (2, round(n ** (1 / 2) + 1)):
        if n%i == 0:
            prost = False
    return prost

def susjedni_do_n(n):
    t = 3
    for i in range(t,20):
        if prost(i) and prost(i + 2):
            print('{} {}'.format(i, i + 2))
    return

def main():
    n = int(input('Unesi broj: '))
    print('Susjedni prosti brojevi do {} su: '.format(n))
    susjedni_do_n(n)
    return

if __name__ == '__main__':
    main()
