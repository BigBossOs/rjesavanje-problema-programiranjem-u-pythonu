print('Cjelobrojno dijeljenje i ostatak')
a=int(input('Unesi prvi broj: '))
b=int(input('Unesi drugi broj: '))
cjelobrojno, ostatak = divmod(a,b)
print('Rezultat dijeljenja brojeva {} i {} je cjelobrojni dio {} i ostatak {}'.format(a,b,cjelobrojno,ostatak))

print('Alternativno racunanje. Zaviri u kod!')
c=a//b
d=a%b
print('Rezultat dijeljenja brojeva {} i {} je cjelobrojni dio {} i ostatak {}'.format(a,b,c,d))
input('Enter za kraj')
