print('Program za izracunavanje opsega i povrsine pravokutnika')
a=float(input('Unesi prvu stranicu pravokutnika: '))
b=float(input('Unesi drugu stranicu pravokutnika: '))
print('Pravokutnik cije su stranice {0:.2f} i {1:.2f} ima opseg {2:.2f} cm i povrsinu {3:.4f} cm2'.format(a,b,2*a+2*b,a*b))
input('Pritisni enter za kraj')
