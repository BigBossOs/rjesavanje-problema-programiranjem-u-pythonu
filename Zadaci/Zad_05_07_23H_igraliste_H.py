# Zad_05_07_23H igraliste
m = int(input('Širina livade: '))
n = int(input('Dužina livade: '))
if n < m:
    n, m = m, n
if n // 2  < m:   # ako je pola dužine livade manje od širine livade
    s, d = n // 2, n # onda je širina igrališta jednaka polovini dužine livade a duljina jednaka duljini igrališta
else:
    s, d = m, 2 * m # inače je širina jednaka šrini livade a duljina jednaka dvostrukoj širini livade
if d < 30:
    print('Igraliste nije moguće sagraditi')
else:
    if d > 80:
        s, d = 40, 80
    print('{} {}'.format(s, d))
