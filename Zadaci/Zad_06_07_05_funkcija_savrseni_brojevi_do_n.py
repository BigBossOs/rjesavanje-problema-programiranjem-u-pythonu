def savrsen(n):
    '''Funkcija vraća True ako je n savršen broj dok u suprotnom vraća False'''
    sum = 0
    for i in range(1,n):
        if not n % i:
            sum += i
    return sum == n

def savrseni_n(n):
    '''Funkcija ispisuje sve savršene brojeve do n'''
    b = 0
    for i in range(1,n+1):
        if savrsen(i):
            b += 1
            print(i)
    if b == 0:
        print('NEMA!')
    return

def main():
   n = int(input('Upisi broj do kojeg zelis provjeriti sve savrsene brojeve: '))
   savrseni_n(n)
   return
   
if __name__ == '__main__':
    main()
