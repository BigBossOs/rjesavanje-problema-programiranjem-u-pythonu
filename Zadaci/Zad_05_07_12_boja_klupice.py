# Zad_05_07_12 boja klupice
n = int(input('Unesi redni broj klupice: '))
if n % 3 == 1:
    print('Klupica s rednim brojem {} treba biti crvena'.format(n))
elif n % 3 == 2:
    print('Klupica s rednim brojem {} treba biti žuta'.format(n))
else:
    print('Klupica s rednim brojem {} treba biti plava'.format(n))
input('Enter za kraj')
