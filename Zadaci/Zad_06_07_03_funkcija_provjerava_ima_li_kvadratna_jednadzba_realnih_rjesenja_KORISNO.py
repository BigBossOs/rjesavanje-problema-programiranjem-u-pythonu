def nultocke(a, b, c):
    '''Funkcija vraća True ukoliko kvadratna jednadžba
    a * x **2 + b * x * c ima realnih rješenja,
    a inače vraća False'''
    return b**2 - 4 * a * c >= 0 #KORISNO       

def main():
    a = int(input('a: '))
    b = int(input('b: '))
    c = int(input('c: '))
    print(nultocke(a,b,c))
    return

if __name__ == '__main__':
    main()
