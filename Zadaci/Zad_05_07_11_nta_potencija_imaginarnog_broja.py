# program za izračunavanje n-te potencije imaginarne jedinice
n = int(input('Unesi eksponent s kojim želiš potencirati imaginanu jedinicu: '))
if n % 4 == 0:
    print('i^{} je: 1'.format(n))
elif n % 4 == 1:
    print('i^{} je: i'.format(n))
elif n % 4 == 2:
    print('i^{} je: -1'.format(n))
else:
    print('i^{} je: -i'.format(n))
input('Enter za kraj')
