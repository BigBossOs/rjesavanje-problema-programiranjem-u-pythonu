# Cekanje na autobusnoj stanici
N = int(input('Koliko minuta je potrebno osobi da dode do stanice A ili B: '))
V = int(input('Koliko minuta je autobusu potrebno da dode do stanice A: '))
M = int(input('Koliko minuta je autobusu potrebno da dode od stanice A do stanice B: '))
if V > N:
    print('Stanica: A\nVrijeme čekanja u minutama: {}'.format(V-N))
else:
    print('Stanica: B\nVrijeme čekanja u minutama: {}'.format(V+M-N))
