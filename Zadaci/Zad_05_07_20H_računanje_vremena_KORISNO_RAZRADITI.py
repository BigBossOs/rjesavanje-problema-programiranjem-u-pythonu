# Zad_05_07_20H računanje vremena
h1 = int(input('Unesi sat početka razgovora: '))
m1 = int(input('Unesi minut početka razgovora: '))
s1 = int(input('Unesi sekundu početka razgovora: '))
h2 = int(input('Unesi sat kraja razgovora: '))
m2 = int(input('Unesi minut kraja razgovora: '))
s2 = int(input('Unesi sekundu kraja razgovora: '))
t = (h2 - h1) * 3600 + (m2 - m1) * 60 + (s2 -s1)
j = t // 15
if t % 15 != 0:
    j += 1
if h1 >= 12:
    n = 50 * j
else:
    n = 40 * j
print('Razgovor koji je počeo u {}:{}:{} i trajao do {}:{}:{} trajao je {} sekundi i bit će naplacen {} lipa'.format(h1,m1,s1,h2,m2,s2,t,n))
