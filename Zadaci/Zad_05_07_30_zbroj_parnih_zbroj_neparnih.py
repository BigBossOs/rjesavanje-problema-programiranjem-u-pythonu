# Zad_05_07_30 zbroj parnih zbroj neparnih
n = int(input('Unesi broj brojeva: '))
zp = 0
zn = 0
for i in range(n):
    broj = int(input('Unesi {}. broj: '.format(i+1)))
    if broj % 2:
        zn += broj
    else:
        zp += broj     
print('Zbroj parnih brojeva: {}\nZbroj neparnih brojeva: {}'.format(zp, zn))
input('Enter za kraj')
