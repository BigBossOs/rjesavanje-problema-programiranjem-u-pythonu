# Zad_05_07_39 n - ti clan geometrijskog niza
def geometrijski(x,y,n):
	q = y / x
	print(round(q ** (n-1)))
	return


a = int(input('Prvi element niza: '))
b = int(input('Drugi element niza: '))
n = int(input('Broj clanova niza: '))
q = b / a
for i in range(n - 1):
	a *= q
print('{}.ti element geometrijskog niza s q = {} je {}'.format(n, q, a))
