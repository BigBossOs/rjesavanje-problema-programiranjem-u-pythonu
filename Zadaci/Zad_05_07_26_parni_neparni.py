# Zad_05_07_26 parni i neparni brojevi
n = int(input('Upisi broj: '))
print('Parni brojevi do broja {}: '.format(n))
for i in range(2,n+1,2):
    print(i, end = ' ')
print()
print('Neparni brojevi do broja {}: '.format(n))
for i in range(1,n+1,2):
    print(i, end = ' ')
print()
print('Prvih {} parnih brojeva: '.format(n))
for i in range(1,n+1):
    print(2 * i, end = ' ')
print()
print('Prvih {} neparnih brojeva: '.format(n))
for i in range(n):
    print(2 * i + 1, end = ' ')
print()
input('Enter za kraj')
