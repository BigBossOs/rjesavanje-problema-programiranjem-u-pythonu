def zbroj(a,b):
    '''Funkcija vraća zbroj dvaju prirodnih brojeva'''
    return a + b

def main():
    a = int(input('Unesi prvi broj: '))
    b = int(input('Unesi drugi broj: '))
    print('Zbroj brojeva je', zbroj(a, b))
    return

if __name__ == '__main__':
    main()
