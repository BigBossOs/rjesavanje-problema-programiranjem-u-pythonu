# Zad_05_07_38 elementi aritmetickog niza od n clanova
def aritmeticki(x,y,n):
	r = y - x
	print(x, y, end =' ')
	for i in range (2,n):
		print(x + i * r, end = ' ')
	return

a = int(input('Prvi element niza: '))
b = int(input('Drugi element niza: '))
n = int(input('Broj clanova niza: '))
d = b - a
print('{} elemenata aritmetickog niza s d = {} su: '.format(n, d))
for i in range(n):
	print(a, end = ' ')
	a += d
