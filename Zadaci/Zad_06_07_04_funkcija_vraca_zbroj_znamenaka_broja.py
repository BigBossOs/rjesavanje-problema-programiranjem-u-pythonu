def zbroj_znamenaka(n):
    '''Funkcija vraća zbroj znamenaka broja'''
    s = 0
    while n > 0:
        s += n % 10    
        n //= 10
    return s

def main():
    n = int(input('Upiši broj: '))
    print('Zbroj znamenaka broja {} je {}'.format(n, zbroj_znamenaka(n)))
    return

if __name__ == '__main__':
    main()
