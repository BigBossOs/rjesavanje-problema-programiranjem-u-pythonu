#Funkcija provjerava je li riječ palindrom; primjer_07_02

def dodaj_crtice(s):
    t = ''
    for i in range(len(s) - 1):
        t += s[i]
        if s[i] == 'A' or s[i] == 'E' or s[i] == 'I' or s[i] == 'O' or s[i] == 'U':
            t += '-'
    t += s[len(s) - 1]
    return t

def dodaj_crtice2(s):
    t = ''
    samogalsnici = 'AEIOU'
    for i in range(len(s) - 1):
        t += s[i]
        if s[i] in samogalsnici:
            t += '-'
    t += s[len(s) - 1]
    return t

def dodaj_crtice3(s):
    t = ''
    for i in range(len(s) - 1):
        t += s[i]
        if s[i] in ('AEIOU'): #ZANIMLJIVO
            t += '-'
    t += s[len(s) - 1]
    return t
