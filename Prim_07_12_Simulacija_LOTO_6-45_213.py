# Simulacija igre LOTO 6/45; primjer_07_12

from random import *
def main():
    # Generiranje svih brojeva od 1 do 45
    brojevi = set(i + 1 for i in range(45))
    # Odabir 6 različitih nasumičnih brojeva do 45
    kombinacija = set(sample(brojevi, 6))
    # Skup unesenih brojeva
    unos = set()
    while len(unos) < 6:
        n = int(input('Unesi prirodan broj do45: '))
        if n in unos or n not in brojevi:
            print('Broj je već unesen, ili nije u zadanom rasponu!')
        unos = unos | {n}
        pogodeni = kombinacija & unos
    if pogodeni:
        print('pogođeno je {} brojeva i to su: {}'.format(len(pogodeni), pogodeni))
    else:
        print('Nije pogođen niti jedan broj')
    print('Dobitna kombinacija: {}'.format(kombinacija))
    return

main()

