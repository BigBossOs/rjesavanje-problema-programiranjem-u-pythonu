# Broji koliko ima prostih brojeva među prvih k prirodnih brojeva; primnjer_05_13_A
import math
k = int(input('Unesi broj: '))
brojac = 0
for n in range(2, k+1):
    prost= True
    for i in range(2, round(math.sqrt(n)+1)):
        if n % i == 0:
            prost = False
    if prost:
        brojac += 1
print('Medu prvih {} prirodnih brojeva ima {} prostih brojeva.'.format(k, brojac))
