# if-elif-else
# Klasifikacija otopina po pH vrijednostima; primjer_05_03
print('''Program će u ovisnosti o utipkanoj vrijednosti napisati
je li otopina jako kisela <0, 4.5], slabo kisela <4.5, 6.5], neutralna <6.5, 7.5], slabo
lužnata <7.5, 9.5] ili jako lužnata <9.5, 14]''')
ph=float(input('Upisati pH vrijednost: '))
if ph > 0 and ph <=4.5:
    print('Otopina s pH-vrijednošću {0:0.1f} je jako kisela.'.format(ph))
elif ph > 4.5 and ph <=6.5:
    print('Otopina s pH-vrijednošću {0:0.1f} je slabo kisela.'.format(ph))
elif ph > 6.5 and ph <=7.5:
    print('Otopina s pH-vrijednošću {0:0.1f} je neutralna.'.format(ph))
elif ph > 7.5 and ph <=9.5:
    print('Otopina s pH-vrijednošću {0:0.1f} je slabo lužnata.'.format(ph))
elif ph > 9.5 and ph <=14.0:
    print('Otopina s pH-vrijednošću {0:0.1f} je jako lužnata.'.format(ph))
else:
    print('zadana vrijednost pH={0:0.1f} nije uobičajena'.format(ph))
