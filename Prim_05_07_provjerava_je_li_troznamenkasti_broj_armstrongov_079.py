# Provjerava je li troznamenkasti broj Armstrongov broj; primjer_05_07
print('''Provjerava je li troznamenkasti broj Armstrongov broj.
Armstrongov je broj koji je jednak zbroju kubova svojih znamenaka''')
n = int(input('Unesi broj: '))
j = n % 10
d = n // 10 % 10
s = n // 100
if n == j**3 + d**3 + s**3:
    print('Broj {0} je Armstrongov broj jer je {1}^3 + {2}^3 + {3}^3 = {0}'.format(n,s,d,j,n))
else:
    print('Broj {} nije Armstrongov broj.'.format(n))
