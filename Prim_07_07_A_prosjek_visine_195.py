# Program unosi broj učenika nekog razreda te visine svih učenika
# Program treba ispisati koliko je učenika viših od prosječne visine učenika tog razreda; primjer_07_07_A
def prosjek(a):
    '''Funkcija vraća aritmetičku sredinu elemenata liste'''
    s = 0
    for i in range(len(a)):
        s += a[i]
    return s / len(a)

def visi_od(a, t):
    brojac = 0
    for i in range(len(a)):
        if a[i] > t:
            brojac += 1
    return brojac

def main():
    n = int(input('Broj ucenika: '))
    visine = []
    for i in range(n):
        v = int(input('Visina {}. učenika: '.format(i + 1)))
        ## Dodavanje elementa na kraj liste
        visine += [v]
        print('{} učenika je više od prosjeka'.format(visi_od(visine, prosjek(visine))))

        
if __name__ == '__main__':
    main()
