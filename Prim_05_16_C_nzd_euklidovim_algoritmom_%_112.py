# Računanje nzd-a dvaju prirodnih brojeva Euklidovim algoritmom; primjer_05_16_C
m = int(input('Prvi broj: '))
n = int(input('Drugi broj: '))
a, b = m, n
while m != 0 and n != 0:
    if m > n:
        m %= n
    else:
        n %=m
if m >0:
    print('nzd brojeva {} i {} je {}'.format(a, b, m))
else:
    print('nzd brojeva {} i {} je {}'.format(a, b, n))
input('Enter za kraj')
