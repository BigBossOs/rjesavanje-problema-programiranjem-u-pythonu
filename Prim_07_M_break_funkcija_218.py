ponoviti_upis = True
while ponoviti_upis:
    slovo = input('Utipkati jedno slovo: ')
    if slovo not in 'abcčćdđefghijklmnoprsštuvzž':
        print('Upisano slovo mora biti malo slovo hrvatske abecede!')
    elif slovo in pogođena_slova or slovo in pogrešna_slova:
        print('Slovo {} je već upisano'.format(slovo))
    else:
        ponoviti_upis = False
prihvaćeno_slovo = slovo

while True:
    slovo = input('Utipkati jedno slovo: ')
    if slovo not in 'abcčćdđefghijklmnoprsštuvzž':
        print('Upisano slovo mora biti malo slovo hrvatske abecede!')
    elif slovo in pogođena_slova or slovo in pogrešna_slova:
        print('Slovo {} je već upisano'.format(slovo))
    else:
        break
prihvaćeno_slovo = slovo

def prihvat_znaka(pogođena_slova, pogrešna_slova):
    '''Funkcija prihvaća utipkano slovo samo ako je malo slovo hrvatske
    abecede i ako prethodno nije bilo utipkano, inače traži ponavljanje
    utipkavanja'''
    while True:
        slovo = input('Utipkati jedno slovo: ')
        if slovo not in 'abcčćdđefghijklmnoprsštuvzž':
            print('Upisano slovo mora biti malo slovo hrvatske abecede!')
        elif slovo in pogođena_slova or slovo in pogrešna_slova:
            print('Slovo {} je već upisano'.format(slovo))
        else:
            return slovo
