# Prvi način izračunavanja srednje visine petljom while; primjer_05_15_B
print('Izračunavanje srednje visine proizvoljnog broja osoba')
brojilo = 0
zbroj = 0.0
ima_brojeva = 'da'
while ima_brojeva == 'da':
    broj = float(input('Upisati visinu u m: '))
    zbroj += broj
    brojilo += 1
    ima_brojeva = input('Ima li još unosa (utipkati da ili ne)?')
print('\nSrednja vrijednost {} osoba u metrima je {:.2f}'.format(brojilo, zbroj / brojilo))
input('Enter za kraj')
