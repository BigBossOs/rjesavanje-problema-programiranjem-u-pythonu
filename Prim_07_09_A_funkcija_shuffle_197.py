# Ilustracija upotrebe naredbe shuffle(); primjer_07_09_A

import random
A = [i for i in range(10)]
print('Izvorna lista')
print('A =', A)
random.shuffle(A)
print('Permutirana lista')
print('A =', A)

