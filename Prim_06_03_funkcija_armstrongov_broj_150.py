# funkcija provjerava je li broj Armstrongov; primjer_06_03
def armstrongov(n):
    '''Funkcija vraća True ako je n Armstrongov, a  inače vrać false'''
    t, s, p = n, 0, 0
    while n > 0:
        n //= 10
        p += 1
    n = t
    while n > 0:
        s += (n % 10) ** p
        n //= 10
    if s == t:
        return True
    else:
        return False

def main():
    '''Funkcija unosi prirodan broj n i ispisuje sve Armstrongove brojeve do n'''
    n = int(input('Unesi prirodan broj: '))
    for i in range(1, n + 1):
        if armstrongov(i):
            print(i)
    return

main()
