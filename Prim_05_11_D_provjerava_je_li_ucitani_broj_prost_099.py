# Provjerava je li učitani broj prost; primjer:05_11_D
import math
n=int(input('Unesi broj: '))
prost = True
for i in range(2,round(math.sqrt(n)+1)):
    if n % i == 0:
        prost = False
if prost == True and n>1:
    print('Broj {} je prost'.format(n))
else:
    print('Broj {} nije prost'.format(n))
