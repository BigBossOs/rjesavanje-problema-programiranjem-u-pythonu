# program unosi dvije točke u obliku (x, y)
# i ispisuje koordinate one točke koja je bliže ishodištu; primjer_07_11_A
from math import sqrt

def tocka(a):
    '''Funkcija pretvara string u par brojeva'''
    # brisanje prve i zadnje zagrade
    a = a.replace('(','')
    a = a.replace(')','')
    # pretvaranje apra brojeva u listu
    t = a.split(',')
    return int(t[0]), int(t[1])

def d(a, b):
    return sqrt(a ** 2 + b ** 2)

def main():
    a = input('A: ')
    b = input('B: ')
    xa, ya = tocka(a)
    xb, yb = tocka(b)
    if d(xa, ya) > d(xb, yb):
        xa, ya, xb, yb = xb, yb, xa, ya
        print('Koordinate točke bliže ishodišta su: ({}, {})'.format(xa, ya))
    return

main()
