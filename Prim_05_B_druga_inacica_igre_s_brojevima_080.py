# Druga inačica igre s brojevima; primjer_05_B
broj_pitanja = 5
print('U ovoj igri trebate odgovoriti na {0} pitanja!'.format(broj_pitanja))
broj_točnih_odgovora = 0
x, y = 17, 25
print('Koliki je zbroj brojeva {0} i {1}'.format(x,y))
z = int(input('Utipkati odgovor: '))
if z == x + y:
    print('Točno!')
    broj_točnih_odgovora+=1
else:
    print('Netočno')

x, y = 11, 12
print('Koliki je umnožak brojeva {0} i {1}'.format(x,y))
z = int(input('Utipkati odgovor: '))
if z == x * y:
    print('Točno!')
    broj_točnih_odgovora+=1
else:
    print('Netočno')

x, y = 27, 56
print('Koliki je zbroj brojeva {0} i {1}'.format(x,y))
z = int(input('Utipkati odgovor: '))
if z == x + y:
    print('Točno!')
    broj_točnih_odgovora+=1
else:
    print('Netočno')

x, y = 15, 17
print('Koliki je umnožak brojeva {0} i {1}'.format(x,y))
z = int(input('Utipkati odgovor: '))
if z == x * y:
    print('Točno!')
    broj_točnih_odgovora+=1
else:
    print('Netočno')

x, y = 28, 68
print('Koliki je zbroj brojeva {0} i {1}'.format(x,y))
z = int(input('Utipkati odgovor: '))
if z == x + y:
    print('Točno!')
    broj_točnih_odgovora+=1
else:
    print('Netočno')

print('\nOdgovorili ste na {} pitanja.\n'.format(broj_pitanja))
print('Od toga je {} točnih.'.format(broj_točnih_odgovora))
postotak = 100* broj_točnih_odgovora / broj_pitanja
print('To znači da imate {:.0f}% točnih odgovora. \n'.format(postotak))
input('Za završetak programa pritisnite tipku <enter> - lijep pozdrav!')
