# Pogađanje zadane riječi - primjer_07

import random

def skrivene_riječi():
    '''U funkciji je definirano područje iz kojeg su odabrane skrivene
    riječi i popis riječi u obliku stringa u kojem su riječi razdvojene
    praznim znakom (bjelinom)'''
    tema = 'CVIJEĆE'
    popis_riječi = '''visibaba maslačak ruža margareta katarinčica
              rododendron šafran kukurijek kaktus dalija
              georgina jaglac čičak'''
    lista_skrivenih_riječi = popis_riječi.split()
    najveći_broj_igara = len(lista_skrivenih_riječi)
    print('''          Igra se može ponoviti najviše {} puta
          jer je zadano toliko skrivenih riječi.'''.format(najveći_broj_igara))
    return tema, lista_skrivenih_riječi, najveći_broj_igara

def prihvat_znaka(pogođena_slova, pogrešna_slova):
    '''Funkcija prihvaća utipkano slovo samo ako je malo slovo hrvatske
    abecede i ako prethodno nije bilo utipkano, inače traži ponavljanje
    utipkavanja'''
    while True:
        slovo = input('Utipkati jedno slovo: ')
        if slovo not in 'abcčćdđefghijklmnoprsštuvzž':
            print('Upisano slovo mora biti malo slovo hrvatske abecede!')
        elif slovo in pogođena_slova or slovo in pogrešna_slova:
            print('Slovo {} je već upisano'.format(slovo))
        else:
            return slovo

def ispis(pogođena_riječ, pogođena_slova, pogrešna_slova):
    '''Funkcija ispisuje string pogođen_riječ s dva prazna znaka
       između svakog slova. '''
    print('\n', 35 * ' ', 'Pogođena slova:', end = ' ')
    for znak in pogođena_slova:
        print(znak, end = ' ')
    print('\n', 35 * ' ', 'Pogrešna slova:', end = ' ')
    for znak in pogrešna_slova:
        print(znak, end = ' ')
    print('\nNepoznata riječ:', end = '     ')
    for znak in pogođena_riječ:
        print(znak, end = ' ')
    print()
    return

def pogađanje(zadana_riječ, maks_br_pog):
    pogođena_riječ = ['_'] * len(zadana_riječ)
    print('\nNepoznata riječ:', end = '   ')
    for znak in pogođena_riječ:
        print(znak, end = ' ')
    print('\n\nDopušta se najviše {} pokušaja pogađanja slova.'\
          .format(maks_br_pog))
    print('Ne ubrajaju se neprihvatljivi znaci i već utipkana slova.\n')
    pogođena_slova = []
    pogrešna_slova = []
    br_pog = 0
    while br_pog < maks_br_pog:
        br_pog += 1
        znak = prihvat_znaka(pogođena_slova, pogrešna_slova)
        if znak in zadana_riječ:
            pogođena_slova.append(znak)
            for i in range(0, len(zadana_riječ)):
                if znak == zadana_riječ[i]:
                    pogođena_riječ[i] = znak
            ispis(pogođena_riječ, pogođena_slova, pogrešna_slova)
            if pogođena_riječ == zadana_riječ:
                return True, br_pog
        else:
            pogrešna_slova.append(znak)
            ispis(pogođena_riječ, pogođena_slova, pogrešna_slova)
    return False, br_pog

def main():
    print('''          Program zadaje nepoznatu riječ.
          Igrač pogađa zadanu riječ utipkavanjem pojedinačnih
          malih slova hrvatske abecede.
          Broj dopuštenih pokušaja jednak je udvostručenom
          broju različitih slova u riječi.
          \n''')
    tema, lista_skrivenih_riječi, najveći_broj_igara = skrivene_riječi()
    broj_igre = 0
    igrati_dalje = True
    while igrati_dalje:
        broj_igre += 1
        if broj_igre > najveći_broj_igara:
            print('\n\nIgranje se prekida jer su sve skrivene riječi iscrpljene.')
            break
        print('\n', 30 * '-',str(broj_igre) + '. IGRA', 30 * '-', 2 *'\n')
        print('TEMA:'.center(70))
        print(tema.center(70))

        # Slučajnimizborom izabire se riječ koja se zatim briše iz liste
        i = random.randint(0, len(lista_skrivenih_riječi) - 1)
        zadana_riječ = list(lista_skrivenih_riječi[i])
        del lista_skrivenih_riječi[i]
        maks_br_pog = 2 * len(set(zadana_riječ))
        pogodak, br_pog = pogađanje(zadana_riječ, maks_br_pog)
        if pogodak:
            print('\n\nBravo!')
            print('U', br_pog, 'pokušaja pogođena je zadana riječ!')
        else:
            print('\n\nNa žalost u', br_pog, 'pogađanja nije pogođena riječ')
            print('"'+''.join(zadana_riječ) + '" duljine',\
                  len(zadana_riječ), 'znakova')
        s = input('\n\nPonoviti igru (da,ne)? : ')
        if s == 'ne':
            igrati_dalje = False
    print('\n\nIgra je završena!')
    return

main()
