# Četvrta završna inačica igre s brojevima; primjer_05_D
import random
import time
random.seed()
nastavak = 'da'
while nastavak == 'da':
    pocetak = time.clock()
    broj_pitanja = random.randint(5,10)
    print('U ovoj igri trebate odgovoriti na {} pitanja!\n'.format(broj_pitanja))
    broj_tocnih_odgovora = 0
    broj_netocnih_odgovora = 0
    for i in range(broj_pitanja):
        x,y = random.randint(5,10), random.randint(5,10)    
        operator = random.randint(0,3)
        upit='Koliki'
        if operator == 0:
            oper = 'umnozak'
            t = x * y
        elif operator == 1:
            oper = 'zbroj'
            t = x + y
        elif operator == 2:
            oper = 'kvocijent'
            while x % y != 0.0:  
               x,y = random.randint(5,20), random.randint(2,10)
            t = x / y #INDENTATION BITAN! ako se uvuce u razini linije iznad ne radi!
        else:
            oper = 'razlika'
            upit= 'Kolika'
            while x < y: 
               x,y = random.randint(5,10), random.randint(2,10)
            t = x - y   #INDENTATION BITAN!
               
        print('{0} je {1} brojeva {2} i {3}?'.format(upit,oper, x, y))
        z = float(input('Utipkati odgovor: '))
        if z == t:
            print('Točno!\n')
            broj_tocnih_odgovora +=1
        else:
            print('Netočno!\n')

    kraj = time.clock()
    print('\nOdgovorili ste na {} pitanja. \n'.format(broj_pitanja))
    print('Od toga je {} točnih.'.format(broj_tocnih_odgovora))
    postotak = 100 * broj_tocnih_odgovora / broj_pitanja
    print('To znači da imate {:.2f} posto točnih odgovora. \n'.format(postotak))
    print('Igru ste igrali {:.2f} sekundi\n'.format(kraj-pocetak))
    nastavak = input('Želite li nastaviti igru (da ili ne)?')
input('Za završetak programa pritisnite <Enter> - lijep pozdrav!')
