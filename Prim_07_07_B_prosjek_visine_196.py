# Program unosi broj učenika nekog razreda te visine svih učenika
# Program treba ispisati koliko je učenika viših od prosječne visine učenika tog razreda; primjer_07_07_B
def prosjek(a):
    '''Funkcija vraća aritmetičku sredinu elemenata liste'''
    s = 0
    for i in range(len(a)):
        s += a[i]
    return s / len(a)

def visi_od(a, t):
    brojac = 0
    for i in range(len(a)):
        if a[i] > t:
            brojac += 1
    return brojac

def main():
    n = int(input('Broj ucenika: '))
    visine = [0] * n
    for i in range(n):
        visine[i] = int(input('Visina {}. učenika: '.format(i + 1)))
        print('{} učenika je više od prosjeka'.format(visi_od(visine, prosjek(visine))))

        
if __name__ == '__main__':
    main()
