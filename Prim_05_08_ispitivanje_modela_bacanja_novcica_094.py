'''Napišimo program za ocjenu generatora nasumičnih brojeva.
Program treba simulirati bacanje novčića n puta te treba ispisati
vjerojatnost ponavljanja pisma odnosno glave.'''
# Ispitivanje modela bacanja novcica; primjer_05_08
from random import randint
print('''Program modelira bacanje novčića s N bacanja i ispisuje
dobivene vjerojatnosti pojavljivanja pisma v_p i glave v_g''')
N = int(input('Upisi zeljeni broj bacanja novcica N: '))
broj_p=0
broj_g=0
for i in range(N):
    bac=randint(0,1)
    if bac == 0:
        broj_p += 1
    else:
        broj_g += 1
v_p = broj_p / N
v_g = broj_g / N
print('Za {0} bacanja novčića utvrđene su sljedeće vjerojatnosti: \nv_p = {1:.8f} \nv_g = {2:.8f}'.format(N, v_p, v_g))
