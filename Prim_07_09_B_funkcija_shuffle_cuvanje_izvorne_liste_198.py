# Ilustracija upotrebe naredbe shuffle() i čuvanje izvorne liste; primjer_07_09_B

import random
def main():
    A = [i for i in range(10)]
    print('\nIzvorna lista')
    print('A =', A)
    B = A[:]
    random.shuffle(B)
    print('Permutirana lista')
    print('B =', B)
    print('\nIzvorna lista se nije promijenila!')
    print('A =', A)
    return
main()

