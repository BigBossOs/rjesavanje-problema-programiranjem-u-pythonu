# Najmanji i najveci element liste od 20 elemenata čiji su elementi
# nasumično generirani prirodni brojevi do 100; primjer_07_06
from random import *
def max_min(a):
    najveci = najmanji = a[0]
    for i in range(1, 20):
        if a[i] > najveci:
            najveci = a[i]
        if a[i] < najmanji:
            najmanji = a[i]
    return najveci, najmanji #vraca dvije odjednom

def main():
    a = [randint(1,100) for i in range(20)]
    print(a)
    veliki, mali = max_min(a)
    print('Najveci: {}\nNajmanji: {}'.format(veliki, mali))
    return

if __name__ == '__main__':
    main()
    
input('Enter za kraj')
