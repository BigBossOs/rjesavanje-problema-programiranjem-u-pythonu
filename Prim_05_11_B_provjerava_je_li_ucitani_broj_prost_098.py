# Provjerava je li učitani broj prost; primjer_05_11_B
n = int(input('Unesi broj: '))
brojac = 0
for i in range(2,n):
    if n % i == 0:
        brojac += 1
if brojac == 0 and n > 1:
    print('Broj {} je prost'.format(n))
else:
    print('Broj {} nije prost'.format(n))
