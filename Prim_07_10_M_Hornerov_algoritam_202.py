# Hornerov algoritam; primjer_07_10_M
def vrijednost_polinoma(p, x0):
    v = 0
    for i in range(len(p)):
        v = v + p[i] * x0 ** i
    return v

def Horner(p, x0):
    v = 0
    p = p[::-1]
    for i in range(len(p)):
        v = v * x0 + p[i]
    return v
