# if-elif-else
# prepoznavanje imena kemijskih spojeva; primjer_05_04
print('''Na temelju upikane kemijske formule program će za poznate mu
spojeve ispisati njihove nazive''')
spoj=input('Upisati kemijsku formulu spoja (primjerice H20): ')
if spoj == 'H20':
    print(spoj,' je voda',sep='') #defaultni separator je sep=' ' 
elif spoj == 'HCL':
    print(spoj,' je solna kiselina',sep='')
elif spoj == 'CH3':
    print(spoj,' je metan',sep='')
elif spoj == 'H2SO4':
    print(spoj,' je sumporna kiselina',sep='')
elif spoj == 'CO2':
    print(spoj,' je ugljikov dioksid',sep='')
else:
    print('Formula {0} mi nije poznata'.format(spoj))
