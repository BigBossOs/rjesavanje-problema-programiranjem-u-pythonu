# Treća inačica igre s brojevima; primjer_05_C
import random
import time
random.seed()
broj_pitanja = random.randint(5,10)
pocetak = time.clock()
print('U ovoj igri trebate odgovoriti na {} pitanja!\n'.format(broj_pitanja))
broj_tocnih_odgovora = 0
broj_netocnih_odgovora = 0
for i in range(broj_pitanja):
    x,y = random.randint(5,10), random.randint(5,10)
    operator = random.randint(0,1)
    if operator:
        oper = 'umnozak'
        t = x * y
    else:
        oper = 'zbroj'
        t = x + y
    print('Koliki je {0} brojeva {1} i {2}?'.format(oper, x, y))
    z = int(input('Utipkati odgovor: '))
    if z == t:
        print('Točno!\n')
        broj_tocnih_odgovora +=1
    else:
        print('Netočno!\n')
kraj = time.clock()
print('\nOdgovorili ste na {} pitanja. \n'.format(broj_pitanja))
print('Od toga je {} točnih.'.format(broj_tocnih_odgovora))
postotak = 100 * broj_tocnih_odgovora / broj_pitanja
print('To znači da imate {} posto točnih odgovora. \n'.format(postotak))
print('Igru ste igrali {:.2f} sekundi\n'.format(kraj-pocetak))
input('Pritisnite tipku <Enter> za kraj')
