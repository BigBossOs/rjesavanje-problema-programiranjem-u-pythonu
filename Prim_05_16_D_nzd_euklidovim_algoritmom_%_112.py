# Računanje nzd-a dvaju prirodnih brojeva Euklidovim algoritmom; primjer_05_16_D
m = int(input('Prvi broj: '))
n = int(input('Drugi broj: '))
a, b = m, n
while m * n != 0:
    if m > n:
        m %= n
    else:
        n %= m
print('nzd brojeva {} i {} je {}'.format(a, b, m + n))
input('Enter za kraj')

