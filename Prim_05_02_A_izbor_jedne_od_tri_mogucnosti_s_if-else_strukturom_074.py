#Za uneseni broj zelimo ustanoviti je li manji od nule, jednak nuli ili veci od nje
#Izbor jedne od tri mogucnosti s if-else strukturom; primjer_05_02_A

broj=int(input('Utipkati zeljeni broj: '))
if broj == 0:
    print('Broj je jednak nuli')
else:
    if broj > 0:
        print('Broj je veci od nule')
    else:
        print('Broj je manji od nule')
