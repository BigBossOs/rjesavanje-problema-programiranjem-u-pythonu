# Ispitivanje funkcije random.randrange(); primjer_07_05
from random import *
print('Deset nasumičnih brojeva generiranih funkcijom randint(0, 10): ')
for i in  range(10):
    print('{:3d}'.format(randint(0, 10)), end = ' ')
print()
print('Deset nasumičnih brojeva generiranih funkcijom randrange(0, 10): ')
for i in  range(10):
    print('{:3d}'.format(randrange(0, 10)), end = ' ')
