# funkcija ispisuje sve proste armstrongove brojeve; primjer_06_04
def prost(n):
    nema_djelitelja = True
    for i in range(2, round(n ** (1/2) +1)):
        if n % i == 0:
            nema_djelitelja = False
    if nema_djelitelja and n>1:
        return True
    else:
        return False

def armstrongov(n):
    '''Funkcija vraća True ako je n Armstrongov, a  inače vraća false'''
    t, s, p = n, 0, 0
    while n > 0:
        n //= 10
        p += 1
    n = t
    while n > 0:
        s += (n % 10) ** p
        n //= 10
    if s == t:
        return True
    else:
        return False

def main():
    '''Funkcija unosi prirodan broj n i ispisuje sve Armstrongove brojeve do n'''
    n = int(input('Unesi prirodan broj: '))
    b = 0
    for i in range(1, n + 1): # ako idemo od 1 onda nikada nećemo dobiti ispis NEMA jer su 2, 3, 5, 7 i armstrongovi i prosti
        if armstrongov(i):
            b += 1
            print(i)
    if b == 0:
        print('NEMA')
    return

if __name__ == '__main__':
    main()
