# Prvo i zadnje slovo riječi ostaje istod dok se ostala shuffleaju; primjer_07_10

from random import shuffle

def razmjena(s):
    ## Znakove riječi od drugog do predzanjeg stavljamo u listu
    l = list(s[1 : len(s) - 1])
    ## elementi liste nasumično se premještaju
    shuffle(l)
    ## kreiramo novu riječ u kojoj prvo stavljamo
    ## prvo slovo riječi, nakon toga razmještena
    ## slova iz liste te zadnje slovo riječi
    novi = s[0]
    for i in range(len(l)):
        novi += l[i]
    novi += s[-1]
    return novi

def main():
    s = input('Unesi riječ: ')
    print (razmjena(s))
    input('Enter za kraj')
    ## ako nema povrata, vrijedost nije nužno pisati, ali se preporučuje
    return

if __name__ == '__main__':
    main()
