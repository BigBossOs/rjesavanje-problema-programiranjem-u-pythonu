def izbaci_višekratnike(brojevi, k):
    '''Funkcija izbacuje sve višekratnike broja k,
        koji su strogo veći od k is skupa brojevi'''
    ##višekratnike broja k dobivat ćemo tako da množimo k
    ##redom s brojevima 2, 3, 4,... sve dok umnožak ne postane
    ##veći od najvećeg elementa skupa
    t = 2
    while t * k <= max(brojevi):
        brojevi = brojevi - {t * k}
        t += 1
    return brojevi

def izbaci_višekratnike2(brojevi, k):
    '''Funkcija izbacuje sve višekratnike broja k,
        koji su strogo veći od k is skupa brojevi'''
    ##višekratnike broja k dobivat ćemo tako da množimo k
    ##redom s brojevima 2, 3, 4,... sve dok umnožak ne postane
    ##veći od najvećeg elementa skupa
    for br in range(2 * k, max(brojevi) + 1, k):
        brojevi = brojevi - {br}
    return brojevi

def sljedeci_element(brojevi, k):
    '''Funkcija vraća prvi element iz skupa veći od k'''
    k += 1
    ##Dok god ne dođemo do broja koji se nalazi u skupu
    ##povećavamo k za 1
    while k not in brojevi:
        k += 1
    return k

def main():
    n = int(input('Do kojeg broja želiš proste bojeve? '))
    brojevi = set(i for i in range(2, n + 1))
    i = 2
    while i < max(brojevi):
        brojevi = izbaci_višekratnike(brojevi, i)
        i = sljedeci_element(brojevi, i)
    print(brojevi)

main()
