# Upotreba if-else strukture na četvrti način; primjer_05_01_C
print('Program će ispisati je li neki utipkani broj djeljiv sa 7')
broj=int(input('Utipkaj prirodni broj: '))
if not broj % 7:
    glagol='je'
else:
    glagol='nije'
print('Broj {} {} djeljiv sa sedam.'.format(broj,glagol))
